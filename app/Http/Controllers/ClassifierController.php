<?php

namespace App\Http\Controllers;

use App\Classifier;
use App\Exports\ClassifierExport;
use App\Http\Resources\Classifier as ClassifierResource;
use App\Http\Requests\Classifier\Import;
use App\Http\Requests\Classifier\Update;
use App\Http\Requests\Classifier\SingleUpdate;
use App\Imports\ClassifierImport;
use App\Services\ConjointApi;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class ClassifierController extends Controller
{
    protected $conjointApi;

    /**
     * ClassifierController constructor.
     * @param ConjointApi $conjointApi
     */
    function __construct(ConjointApi $conjointApi)
    {
        $this->conjointApi = $conjointApi;
    }


    /**
     * Возвращяет все записи Classifier
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getClassifiers()
    {
        return ClassifierResource::collection(Classifier::all());
    }


    /**
     * Сохраняет все записи, отправляет из в API Conjoint и возвращает коллекцию ресурсов
     * с дополнительными полями
     *
     * @param Update $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     * @throws \Exception
     */
    public function updateClassifiers(Update $request)
    {
        // сохраним полченные данные и получим их коллекцию
        $classifiers = collect($request->classifiers)
            ->map( function ($item) {
                return Classifier::find($item['id'])
                    ->updateItem($item);
        });

        // отправим запрос к API и вернем результат
        return ClassifierResource::collection(
            $this->conjointApi
                ->checkData($classifiers)
                ->map(function ($item) {
                    $classifier = Classifier::find($item['id']);
                    $classifier->conjointTopic = $item['topic'];
                    $classifier->source = $item['source'];
                    return $classifier;
                })
        );
    }


    /**
     * Обновление одной записи и возврат ее ресурса
     *
     * @param SingleUpdate $request
     * @param Classifier $classifier
     * @return ClassifierResource
     */
    public function updateSingleClassifiers(SingleUpdate $request, Classifier $classifier)
    {
        return new ClassifierResource($classifier->updateItem($request->all()));
    }

    /**
     * Формирует и эксортирует файл CSV с данными из БД
     *
     * @param ClassifierExport $classifierExport
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function exportClassifiers(ClassifierExport $classifierExport)
    {
        return Excel::download($classifierExport, date('Ymd_') . 'export.csv', \Maatwebsite\Excel\Excel::CSV,
            [
                'Content-Type' => 'text/csv',
            ]);
    }

    /**
     * Импорт данных из файла CSV
     *  сохранение файла и уделение после импорта, связанно с тем, что по какой то причине в каталог
     *  временных фалов, файл загружется с расширением bin, хотя все валидации проходят.
     *  С файлами *.bin Maatwebsite/Excel рабоать не может, так как внутри выполнятеся проверка на
     *  содержимое файли на расширение файла.
     *
     * @param Import $request
     * @param ClassifierImport $classifierImport
     * @param Excel $excel
     * @param Storage $storage
     */
    public function importClassifiers(Import $request, ClassifierImport $classifierImport)
    {
        $storedFile = $request->file('file')
            ->storeAs(
                'public',
                $request->file('file')
                    ->getFilename() . '.csv');
        Excel::import($classifierImport, $storedFile);
        Storage::delete($storedFile);
    }
}
