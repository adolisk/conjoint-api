<?php

namespace App\Http\Requests\Classifier;

use Illuminate\Foundation\Http\FormRequest;

class SingleUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Валидация одной записи
     *
     * @return array
     */
    public function rules()
    {
        return [
            'text'  => 'required|string|max:255',
            'topic' => 'required|string'
        ];
    }
}
