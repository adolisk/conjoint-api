<?php

namespace App\Http\Requests\Classifier;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * валидация массива записей
     *
     * @return array
     */
    public function rules()
    {
        return [
            'classifiers'         => 'required|array',
            'classifiers.*.id'    => 'required|integer',
            'classifiers.*.text'  => 'required|string|max:255',
            'classifiers.*.topic' => 'nullable|string',
        ];
    }
}
