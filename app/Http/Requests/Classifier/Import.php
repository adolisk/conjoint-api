<?php

namespace App\Http\Requests\Classifier;

use Illuminate\Foundation\Http\FormRequest;

class Import extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Валидация файла импорта
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file'   => 'required',
            'file.*' => 'required|mimes:csv,txt'
        ];
    }
}
