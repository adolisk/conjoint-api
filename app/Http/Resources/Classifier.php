<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Classifier extends JsonResource
{
    /**
     * представление модели для отправки через API
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'text'          => $this->text,
            'topic'         => $this->topic,
            'conjointTopic' => $this->conjointTopic ?: $this->topic,
            'source'        => $this->source
        ];
    }
}
