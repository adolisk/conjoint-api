<?php

namespace App\Imports;

use App\Classifier;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ClassifierImport implements ToModel, WithHeadingRow
{
    /**
     * Импорт в модель, если такая запись уже есть, то обновляем ее из файла импорта
     * @param array $row
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Model[]|null
     */
    public function model(array $row)
    {
        $classifier = Classifier::findOrNew($row['id']);
        $classifier->id    = $row['id'];
        return $classifier->updateItem([
            'text'  => preg_replace('/[\x00-\x09\x0B\x0C\x0E-\x1F\x7F]/', '', $row['text']),
            'topic' => preg_replace('/[\x00-\x09\x0B\x0C\x0E-\x1F\x7F]/', '', $row['topic'])
        ]);
    }

    /**
     * Указываем после какой строки начинаются данные и заканчиваются заголовки
     * @return int
     */
    public function headingRow(): int
    {
        return 1;
    }
}
