<?php

namespace App\Exports;

use App\Classifier;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ClassifierExport implements FromCollection, WithHeadings
{
    /**
     * Выборка записей для файла экспорта
     *
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Classifier::all();
    }

    /**
     * Задаем заголовки для файла экспорта
     *
     * @return array
     */
    public function headings(): array
    {
        return [
            'id',
            'text',
            'topic',
        ];
    }
}
