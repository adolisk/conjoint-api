<?php


namespace App\Services;

use Illuminate\Support\Collection;

class ConjointApi
{
    /**
     * Спрашиваем у API conjoint, если запрос был не удачные, то вызывем исключение
     *
     * @param array $postData
     * @return mixed
     * @throws \Exception
     */
    protected function sendData(array $postData)
    {
        $curl = curl_init(config('app.conjoint_api'));

        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode([
                'command' => 'classifier-test',
                'input' => $postData
        ]));
        curl_setopt($curl, CURLOPT_HEADER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'X-Token: TEST'
        ]);

        $response = curl_exec($curl);

        $response_code = curl_getinfo($curl,  CURLINFO_HTTP_CODE);
        if ($response_code === 200) {
            return json_decode(substr($response, curl_getinfo($curl, CURLINFO_HEADER_SIZE)), true);
        }
        throw new \Exception('Something wrong...',$response_code);
    }

    /**
     * Получаем от отдаем коллекцию
     *
     * @param Collection $data
     * @return Collection
     * @throws \Exception
     */
    public function checkData(Collection $data)
    {
        return collect($this->sendData($data->toArray())['result']['result']);

    }
}
