<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classifier extends Model
{
    protected $fillable = ['id', 'text', 'topic'];

    /** @var bool отключем timestamps */
    public $timestamps = false;

    /**
     * Обновляем данные и возвращаем экземпляр модели
     *
     * @param array $data
     * @return Classifier
     */
    public function updateItem(array $data)
    {
        $this->text  = $data['text'];
        $this->topic = $data['topic'] ?: '';
        return !$this->save() ?: $this;
    }
}
