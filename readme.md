## Install

```
git clone https://z3d_909@bitbucket.org/adolisk/conjoint-api.git

cd conjoint-api/
cp .env.example .env
composer install
php artisan key:generate
php artisan storage:link

//next you have to setup database connection in .env

php artisan migrate
```

##### Start server
```
php artisan serve
```

##### Virtualhost config for Apache
```
<VirtualHost *:80>
	ServerName api.conjoint.local
	ServerAlias api.conjoint.local
	ServerAdmin webmaster@localhost
        RewriteEngine On
	DocumentRoot /var/www/conjoint-api/public

	<Directory /var/www/conjoint-api/public>
		AllowOverride All
		Order allow,deny
		Allow from all
	</Directory>

	ErrorLog ${APACHE_LOG_DIR}/error.log
	CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
``` 
