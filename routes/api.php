<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/classifiers',                      'ClassifierController@getClassifiers');
Route::post('/classifiers/update',              'ClassifierController@updateClassifiers');
Route::post('/classifiers/update/{classifier}', 'ClassifierController@updateSingleClassifiers');
Route::get('/classifiers/export',               'ClassifierController@exportClassifiers');
Route::post('/classifiers/import',              'ClassifierController@importClassifiers');
